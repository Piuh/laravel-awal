<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index');

Route::get('/register','AuthController@register');

Route::post('/post','AuthController@post');

Route::get('/master', function(){
  return view('master');
});

route::get('/items', function(){
    return view('items.index');
});

route::get('/', function(){
    return view('tugas./');
});

route::get('/data-tables', function(){
    return view('tugas.data-tables');
});
